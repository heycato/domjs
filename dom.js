;((win, doc) => {

const tags = [ "a", "abbr", "acronym", "address", "applet", "area", "article", "aside", "audio", "b", "base", "basefont", "bdi", "bdo", "big", "blockquote", "body", "br", "button", "canvas", "caption", "center", "cite", "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog", "dir", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "font", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hr", "html", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li", "link", "main", "map", "mark", "menu", "menuitem", "meta", "meter", "nav", "noframes", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strike", "strong", "style", "sub", "summary", "sup", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "tt", "u", "ul", "var", "video"] 
const attributeExceptions = ['role', 'attribute']

const childAppender = (win, doc, parent, child) => {
  if(Array.isArray(child)) {
    child.reduce(childAppender.bind(null, win, doc), parent)
  } else if(child instanceof win.Element) {
    parent.appendChild(child)
  } else if(typeof child === 'string') {
    parent.appendChild(doc.createTextNode(child))
  }
  return parent
}

const setStyles = (el, styles) => {
  if(!styles) {
    el.removeAttribute('styles')
    return el
  }
  Object.keys(styles)
    .map((prop) => {
      if(prop in el.style) {
        el.style[prop] = styles[prop]
      } else {
        console.warn(`${styleName} is not a valid style for a <${el.tagName.toLowerCase()}>`);
      } 
    })
  return el
}

const setAttributes = (el, attrs) => {
  Object.keys(attrs)
    .map((prop) => {
      if(attrs[prop] === undefined) {
        el.removeAttribute(prop)
      } else {
        el.setAttribute(prop, attrs[prop])
      }
    })
  return el
}

const createElement = (win, doc, type, textOrPropsOrChild, ...otherChildren) => {
  const el = doc.createElement(type)
  const appendChild = childAppender.bind(null, win, doc)

  if(Object.prototype.toString.apply(textOrPropsOrChild) === '[object Object]') {
    Object.keys(textOrPropsOrChild)    
      .map((name) => {
        if(name in el || attributeExceptions.includes(name)) {
          const value = textOrPropsOrChild[name]
          if(name === `style`) {
            setStyles(el, value)
          } else if(name === `attribute`) {
            setAttributes(el, value)
          } else if(value) {
            el[name] = value
          }
        } else {
          console.warn(`${name} is not a valid property of a <${type}>`)
        }
      })
  } else {
    appendChild(el, textOrPropsOrChild)
  }
  
  if(otherChildren) appendChild(el, otherChildren)
  return el
}

win.domjs = tags.reduce((acc, tag) => {
    acc[tag] = createElement.bind(null, win, doc, tag)
    return acc
  }, {})

})(window, document)
